import requests


def get_session():
    """
    Creating a requests sessoin with proxy ip address.
    Depends on proxy service.

    :return: Configurated requests session instance
    """
    session = requests.session()
    session.proxies.update({
        'http': 'socks5h://tor:9050',
        'https': 'socks5h://tor:9050'
    })
    session.headers.update({'User-Agent': 'Firefox/3.5.8'})
    return session
