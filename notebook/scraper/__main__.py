import json
from argparse import ArgumentParser
from itertools import islice

import requests

from search import get_city_id
from diary import scrap_temperature


parser = ArgumentParser()
parser.add_argument('-c', '--city_name', type=str, default='Томск',
                    help='Sets city name for search city_id')
parser.add_argument('-a', '--api', type=str, required=True,
                    help='Sets destination api endpoint')
parser.add_argument('-y', '--year', type=str, default=2018,
                    help='Sets year for scraping')
args = parser.parse_args()

session = requests.session()
session.headers.update({'content-type': 'application/json'})

city_id = get_city_id(args.city_name)
data = scrap_temperature(city_id, args.year)

while True:
    batch_data = list(islice(data, 0, 100))

    if not batch_data:
        break

    resp = session.post(
        args.api, data=json.dumps(batch_data)
    )
    print(resp.status_code)
    print(resp.text)
