from math import ceil

from lxml.html import fromstring
from funcy import last

from session import get_session


def _get_lost_value(elements):
    """
    Missing values adding function.

    :param elements: lxml elements collection
    :return: Generator that contains month temperature list with missing values
    """
    last_value = None
    for element in elements:
        value = last_value if element.text is None else int(element.text)
        yield ceil(value)
        last_value = value


def scrap_temperature(city_id, year):
    """
    Temperature scrapping function for year.
    :param city_id: Gismeteo city id
    :param year: Scrapping temperature year
    :return: Generator that contains year temperature list
    """
    session = get_session()
    for month in range(1, 13):
        city_url = f'https://gismeteo.ru/diary/{city_id}/{year}/{month}'
        response = session.get(city_url)

        tree = fromstring(response.text)
        body = last(tree.xpath('//table/tbody'))

        number = _get_lost_value(body.xpath('.//tr/td[1]'))
        day_temperature = _get_lost_value(body.xpath('.//tr/td[2]'))
        night_rermperature = _get_lost_value(body.xpath('.//tr/td[7]'))

        day_data = zip(number, day_temperature, night_rermperature)

        for number, day_temp, night_temp in day_data:
            yield {
                'date': f'{year}-{month}-{number}',
                'temperature': ceil(sum((day_temp, night_temp)) / 2)
            }
