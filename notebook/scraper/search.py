from functools import reduce

from transliterate import slugify
from lxml.html import fromstring
from funcy import first, last

from session import get_session


def get_city_id(city_name):
    """
    City id scrapping function.

    :param city_name: Cyrillic city name
    :return: Gismeteo city id
    """
    city_slug = slugify(city_name).lower()
    session = get_session()
    search_url = f'https://gismeteo.com/search/{city_name}'
    response = session.get(search_url)

    tree = fromstring(response.text)

    blocks = tree.xpath('//div[@class="catalog_list"]')
    items = reduce(
        lambda value, item: value + item,
        [block.xpath('.//div[@class="catalog_item"]') for block in blocks],
        []
    )
    links = reduce(
        lambda value, item: value + item,
        [item.xpath('.//a[contains(@class, "catalog_link")]') for item in items],
        []
    )

    addresses = {link.get('href').strip('/') for link in links}
    city_path_url = first(
        sorted(
            (address for address in addresses if city_slug in address and address.startswith('weather'))
        )
    )

    return last(city_path_url.split('-'))
