from fabric.api import local, settings


PROJECT_NAME = 'scraper'


def createserver():
    name = f'{PROJECT_NAME}_server'
    with settings(warn_only=True):
        local(f'docker-compose build server\n'
              f'docker rm -f {name}\n'
              f'docker create --name {name} {name}\n'
              f'docker cp {name}:/home/python/app/poetry.lock ./server/poetry.lock\n'
              f'docker rm -f {name}')


def runserver():
    local(f'docker-compose run --service-ports --use-aliases server poetry run python manage.py runserver 0.0.0.0:8000')


def test():
    local(f'docker-compose run --use-aliases server poetry run pytest -s .')


def bash(service_name):
    service_id = f"$(docker container ls | grep {PROJECT_NAME}_{service_name} | awk '{{print $1}}')"
    local(f'docker exec -it {service_id} bash')


def makemigrations(app_name=None):
    server_id = f"$(docker container ls | grep {PROJECT_NAME}_server | awk '{{print $1}}')"
    if app_name:
        local(f'docker exec -i {server_id} poetry run python manage.py makemigrations --empty {app_name}')
    else:
        local(f'docker exec -i {server_id} poetry run python manage.py makemigrations')


def mergemigrations():
    server_id = f"$(docker container ls | grep {PROJECT_NAME}_server | awk '{{print $1}}')"
    local(f'docker exec -i {server_id} poetry run python manage.py makemigrations --merge')


def migrate():
    server_id = f"$(docker container ls | grep {PROJECT_NAME}_server | awk '{{print $1}}')"
    local(f'docker exec -i {server_id} poetry run python manage.py migrate')


def shell():
    server_id = f"$(docker container ls | grep {PROJECT_NAME}_server | awk '{{print $1}}')"
    local(f'docker exec -it {server_id} poetry run python manage.py shell')


def startapp(app_name):
    service_id = f"$(docker container ls | grep {PROJECT_NAME}_server | awk '{{print $1}}')"
    local(f'docker exec -it {service_id} poetry run python manage.py startapp {app_name}')


def createsuperuser():
    server_id = f"$(docker container ls | grep {PROJECT_NAME}_server | awk '{{print $1}}')"
    local(f'docker exec -it {server_id} poetry run python manage.py createsuperuser')
