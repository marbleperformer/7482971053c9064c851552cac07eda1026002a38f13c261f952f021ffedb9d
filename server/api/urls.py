from django.urls import path, include
from rest_framework.routers import DefaultRouter

from weather.viewsets import ObservationViewSet


app_name = 'api'


router = DefaultRouter()
router.register('observations', ObservationViewSet, 'observations')

urlpatterns = [
    path('', include(router.urls), name='v1')
]
