SECRET_KEY = 'scraper'

# Application definition
#
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',

    'rest_framework',
    'django_filters',

    'filterbackends',

    'weather',
    'tests',
    'api',
]

# Database definition
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    }
}

MIDDLEWARE = []

ROOT_URLCONF = 'tests.urls'
