from rest_framework.serializers import ModelSerializer

from ..models import BaseModel


class BaseModelSerializer(ModelSerializer):
    model = BaseModel
    fields = ['id']
