from rest_framework.serializers import ModelSerializer

from ..models import TestModel


class RangeModelSerializer(ModelSerializer):
    class Meta:
        model = TestModel
        fields = ['id', 'date']
