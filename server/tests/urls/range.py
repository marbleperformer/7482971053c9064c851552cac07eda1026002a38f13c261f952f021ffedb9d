from django.urls import path, include
from rest_framework.routers import DefaultRouter

from ..viewsets import RangeModelViewSet


app_name = 'range'


router = DefaultRouter()
router.register('range', RangeModelViewSet, 'range')


urlpatterns = [
    path('api/', include(router.urls))
]
