from django.urls import path, include


urlpatterns = [
    path('rarnge/', include('tests.urls.range')),
    path('list/', include('tests.urls.list')),
    path('base/', include('tests.urls.base')),
    path('api/', include('api.urls')),
]
