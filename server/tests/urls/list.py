from django.urls import path, include
from rest_framework.routers import DefaultRouter

from ..viewsets import ListModelViewSet


app_name = 'list'


router = DefaultRouter()
router.register('list', ListModelViewSet, 'list')


urlpatterns = [
    path('api/', include(router.urls))
]
