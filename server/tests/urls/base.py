from django.urls import path, include
from rest_framework.routers import DefaultRouter

from ..viewsets import NotImplementedViewSet


app_name = 'base'


router = DefaultRouter()
router.register('notimplemented', NotImplementedViewSet, 'notimplemented')


urlpatterns = [
    path('api/', include(router.urls))
]
