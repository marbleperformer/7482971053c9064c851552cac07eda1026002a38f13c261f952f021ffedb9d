from datetime import datetime, timedelta

from django.test import Client
from rest_framework.reverse import reverse

import pytest
from mixer.backend.django import mixer

from ..models import TestModel


@pytest.yield_fixture
def test_db_setup():
    dates = (datetime.now().date() + timedelta(days=idx) for idx in range(-40, 40, 4))
    yield mixer.cycle(20).blend(TestModel, date=dates)
    TestModel.objects.all().delete()


@pytest.mark.django_db
def test_ivalid_filter(test_db_setup):
    path = reverse('range:range-list')
    response = Client().get(path, {'date__gte': 123})
    assert response.status_code == 400


@pytest.mark.django_db
def test_regular_request(test_db_setup):
    path = reverse('range:range-list')
    response = Client().get(path)
    assert len(response.data) == 20


@pytest.mark.django_db
def test_lte_filter(test_db_setup):
    path = reverse('range:range-list')
    date = datetime.now().date() - timedelta(days=16)
    response = Client().get(path, {'date__lte': date.strftime('%Y-%m-%d')})
    assert len(response.data) == 7


@pytest.mark.django_db
def test_gte_filter(test_db_setup):
    path = reverse('range:range-list')
    date = datetime.now() + timedelta(days=24)
    response = Client().get(path, {'date__gte': date.strftime('%Y-%m-%d')})
    assert len(response.data) == 4


@pytest.mark.django_db
def test_range_filter(test_db_setup):
    path = reverse('range:range-list')
    date_start = datetime.now() - timedelta(days=16)
    date_end = datetime.now() + timedelta(days=24)
    date_range_params = {
        'date__gte': date_start.strftime('%Y-%m-%d'),
        'date__lte': date_end.strftime('%Y-%m-%d')
    }
    response = Client().get(path, date_range_params)
    assert len(response.data) == 11


@pytest.mark.django_db
def test_empty_gte_filter(test_db_setup):
    path = reverse('range:range-list')
    response = Client().get(path, {'date__gte': ''})
    assert len(response.data) == 20


@pytest.mark.django_db
def test_empty_lte_filter(test_db_setup):
    path = reverse('range:range-list')
    response = Client().get(path, {'date__lte': ''})
    assert len(response.data) == 20


@pytest.mark.django_db
def test_empty_range_filter(test_db_setup):
    path = reverse('range:range-list')
    date_range_params = {'date__gte': '', 'date__lte': ''}
    response = Client().get(path, date_range_params)
    assert len(response.data) == 20
