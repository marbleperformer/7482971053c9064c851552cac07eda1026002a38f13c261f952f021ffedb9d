from datetime import datetime, timedelta

from django.test import Client
from rest_framework.reverse import reverse

import pytest
from mixer.backend.django import mixer

from ..models import TestModel


@pytest.yield_fixture
def test_db_setup():
    dates = (datetime.now().date() + timedelta(days=idx) for idx in range(-40, 40, 4))
    yield mixer.cycle(20).blend(TestModel, date=dates)
    TestModel.objects.all().delete()


@pytest.mark.django_db
def test_ivalid_filter(test_db_setup):
    path = reverse('list:list-list')
    response = Client().get(path, {'date__in': 123})
    assert response.status_code == 400


@pytest.mark.django_db
def test_regular_request(test_db_setup):
    path = reverse('list:list-list')
    response = Client().get(path)
    assert len(response.data) == 20


@pytest.mark.django_db
def test_in_filter(test_db_setup):
    path = reverse('list:list-list')
    dates = (datetime.now().date() + timedelta(days=idx) for idx in range(-20, 20, 4))
    dates = (date.strftime('%Y-%m-%d') for date in  dates)
    response = Client().get(path, {'date__in': dates})
    assert len(response.data) == 10


@pytest.mark.django_db
def test_empty_in_filter(test_db_setup):
    path = reverse('list:list-list')
    response = Client().get(path, {'date__in': ''})
    assert len(response.data) == 20
