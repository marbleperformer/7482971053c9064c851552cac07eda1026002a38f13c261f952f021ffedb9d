from django.test import Client
from rest_framework.reverse import reverse

import pytest


def test_list_request():
    with pytest.raises(NotImplementedError):
        path = reverse('base:notimplemented-list')
        Client().get(path)
