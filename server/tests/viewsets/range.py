from rest_framework.viewsets import ModelViewSet

from filterbackends.backends import RangeFilterBackend

from ..serializers import TestModelSerializer
from ..models import TestModel


class RangeModelViewSet(ModelViewSet):
    queryset = TestModel.objects.all()
    serializer_class = TestModelSerializer
    filter_backends = [RangeFilterBackend]
    range_filterset_fields = ['date']
