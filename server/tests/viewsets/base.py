from rest_framework.viewsets import ModelViewSet

from filterbackends.backends import BaseFilterBackend

from ..serializers import BaseModelSerializer
from ..models import BaseModel


class NotImplementedViewSet(ModelViewSet):
    queryset = BaseModel.objects.all()
    serializer_class = BaseModelSerializer
    filter_backends = [BaseFilterBackend]
