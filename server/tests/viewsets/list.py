from rest_framework.viewsets import ModelViewSet

from filterbackends.backends import ListFilterBackend

from ..serializers import TestModelSerializer
from ..models import TestModel


class ListModelViewSet(ModelViewSet):
    queryset = TestModel.objects.all()
    serializer_class = TestModelSerializer
    filter_backends = [ListFilterBackend]
    list_filterset_fields = ['date']
