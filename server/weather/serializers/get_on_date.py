from django.utils import timezone
from rest_framework import serializers


class GetOnDateSerializer(serializers.Serializer):
    date = serializers.DateField(required=False, default=timezone.now().date)
