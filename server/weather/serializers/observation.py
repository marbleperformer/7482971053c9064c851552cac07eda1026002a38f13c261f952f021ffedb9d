from rest_framework import serializers

from ..models import Observation


class ObservationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Observation
        fields = ['id', 'temperature', 'date']


class ObservationUploadSerializer(serializers.ModelSerializer):
    date = serializers.DateField()

    class Meta:
        model = Observation
        fields = ['temperature', 'date']
