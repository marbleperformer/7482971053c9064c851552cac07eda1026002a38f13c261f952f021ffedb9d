from datetime import datetime, timedelta

from django.test import Client
from rest_framework.reverse import reverse

import pytest
from mixer.backend.django import mixer

from ..serializers import ObservationSerializer
from ..models import Observation


@pytest.yield_fixture
def weather_db_setup():
    dates = (datetime.now().date() + timedelta(days=idx) for idx in range(-40, 40, 4))
    temperatures = (item for item in range(-20, 20))
    yield mixer.cycle(20).blend(Observation, date=dates, temperature=temperatures)
    Observation.objects.all().delete()


@pytest.mark.django_db
def test_get_on_date(weather_db_setup):
    date = datetime.now().strftime('%Y-%m-%d')
    path = reverse('api:observations-get-on-date', kwargs={'date':date})

    response = Client().get(path)

    instance = Observation.objects.get(date=datetime.now())
    data = ObservationSerializer(instance, context={'request': response.wsgi_request}).data

    assert response.data == data


@pytest.mark.django_db
def test_get_on_date(weather_db_setup):
    date = datetime.now().strftime('%Y-%m-%d')
    path = reverse('api:observations-get-on-date', kwargs={'date':date})

    response = Client().get(path)
    instance = Observation.objects.get(date=datetime.now())
    data = ObservationSerializer(instance, context={'request': response.wsgi_request}).data

    assert response.data == data


@pytest.mark.django_db
def test_404_get_on_date(weather_db_setup):
    path = reverse('api:observations-get-on-date', kwargs={'date':'1000-01-01'})
    response = Client().get(path)
    assert response.status_code == 404


@pytest.mark.django_db
def test_wrong_format(weather_db_setup):
    path = reverse('api:observations-get-on-date', kwargs={'date':'1000-01-41'})
    response = Client().get(path)
    assert response.status_code == 400
