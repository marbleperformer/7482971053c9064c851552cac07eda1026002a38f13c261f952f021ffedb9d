import json
from datetime import datetime, timedelta

from django.test import Client
from rest_framework.reverse import reverse

import pytest
from mixer.backend.django import mixer

from ..models import Observation


@pytest.yield_fixture
def epmpty_db_setup():
    yield None
    Observation.objects.all().delete()


@pytest.yield_fixture
def weather_db_setup():
    dates = (datetime.now().date() + timedelta(days=idx) for idx in range(-40, 40, 4))
    temperatures = (item for item in range(-20, 20))
    yield mixer.cycle(20).blend(Observation, date=dates, temperature=temperatures)
    Observation.objects.all().delete()


@pytest.fixture
def upload_data():
    dates = (datetime.now().date() + timedelta(days=idx)
             for idx in range(500))
    return [
        {
            'date': date.strftime('%Y-%m-%d'),
            'temperature': num
        }
        for num, date in enumerate(dates, start=1)
    ]


@pytest.fixture
def corrupded_data():
    return [
        {'date': idx, 'temperature': idx}
        for idx in range(500)
    ]


@pytest.mark.django_db
def test_upload(upload_data, epmpty_db_setup):
    path = reverse('api:observations-upload')
    response = Client().post(path, json.dumps(upload_data), content_type='application/json')

    assert response.status_code == 200
    assert Observation.objects.count() == 500


@pytest.mark.django_db
def test_upload(corrupded_data, epmpty_db_setup):
    path = reverse('api:observations-upload')
    response = Client().post(path, json.dumps(corrupded_data), content_type='application/json')

    assert response.status_code == 400
