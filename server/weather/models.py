from django.db import models


class Observation(models.Model):
    temperature = models.IntegerField()
    date = models.DateField(unique=True)

    def __str__(self):
        return self.date.strftime('%Y-%b-%d')
