from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_400_BAD_REQUEST
from rest_framework.response import Response

from filterbackends.backends import RangeFilterBackend, ListFilterBackend

from .models import Observation
from .serializers import (
    ObservationSerializer, ObservationUploadSerializer, GetOnDateSerializer
)


class ObservationViewSet(ModelViewSet):
    queryset = Observation.objects.all()
    pagination_class = LimitOffsetPagination
    filter_backends = [
        DjangoFilterBackend, RangeFilterBackend, ListFilterBackend
    ]
    filterset_fields = ['temperature', 'date']
    range_filterset_fields = ['date']
    list_filterset_fields = ['date']

    def get_serializer_class(self):
        if self.action == 'upload':
            return ObservationUploadSerializer
        return ObservationSerializer


    @action(detail=False, methods=['GET'],
            url_path=r'(?P<date>\d{4}-\d{2}-\d{2})',
            url_name='get-on-date')
    def get_on_date(self, request, date):
        serializer = GetOnDateSerializer(data={'date': date})

        if not serializer.is_valid():
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

        date = serializer.data.get('date')

        queryset = self.get_queryset().filter(date=date)

        if not queryset.exists():
            return Response(
                {'get_on_date':f'Observation with date {date} not found.'},
                status=HTTP_404_NOT_FOUND
            )

        serializer = self.get_serializer(queryset.first())
        return Response(serializer.data, status=HTTP_200_OK)

    @action(detail=False, methods=['POST'])
    def upload(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(data=request.data, many=True)

        if not serializer.is_valid():
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

        bulk_data = map(
            lambda itm: queryset.model(**itm),
            serializer.data
        )

        queryset.model.objects.bulk_create(bulk_data, ignore_conflicts=True)

        return Response(status=HTTP_200_OK)
