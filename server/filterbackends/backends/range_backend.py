from django.template.loader import render_to_string
from django_filters.filterset import FilterSet

from .base_backend import BaseFilterBackend


class RangeFilterBackend(BaseFilterBackend):
    def get_filterset_class(self, queryset, view):
        filterset_fields = getattr(view, 'range_filterset_fields', [])
        class AutoFilterSet(FilterSet):
            class Meta:
                model = queryset.model
                fields = {field: ['gte', 'lte'] for field in filterset_fields}
        return AutoFilterSet

    def to_html(self, request, queryset, view):
        filterset = self.get_filterset(request, queryset, view)
        return render_to_string(
            'filterbackends/rage-filter.html',
            {'filter': filterset}
        )
