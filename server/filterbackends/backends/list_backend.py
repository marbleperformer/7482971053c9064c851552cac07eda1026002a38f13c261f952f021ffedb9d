from django_filters.filterset import FilterSet

from .base_backend import BaseFilterBackend


class ListFilterBackend(BaseFilterBackend):
    def get_filterset_kwargs(self, request, queryset):
        data = {key: ','.join(request.GET.getlist(key))
                for key in request.GET}
        return {
            'data': data,
            'queryset': queryset
        }

    def get_filterset_class(self, queryset, view):
        filterset_fields = getattr(view, 'list_filterset_fields', [])
        class ModelFilterSet(FilterSet):
            class Meta:
                model = queryset.model
                fields = {field: ['in'] for field in filterset_fields}
        return ModelFilterSet
