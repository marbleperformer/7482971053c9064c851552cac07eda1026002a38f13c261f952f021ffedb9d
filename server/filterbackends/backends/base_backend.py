from rest_framework.filters import BaseFilterBackend as DjangoFilterBackend
from django_filters.utils import translate_validation


class BaseFilterBackend(DjangoFilterBackend):
    def get_filterset_class(self, view, queryset):
        raise NotImplementedError

    def get_filterset(self, request, queryset, view):
        kwargs = self.get_filterset_kwargs(request, queryset)
        filterset_class = self.get_filterset_class(queryset, view)
        return filterset_class(**kwargs)

    def filter_queryset(self, request, queryset, view):
        filterset = self.get_filterset(request, queryset, view)
        if not filterset.is_valid():
            raise translate_validation(filterset.errors)
        return filterset.qs

    def get_filterset_kwargs(self, request, queryset):
        return {
            'data': request.GET,
            'queryset': queryset
        }
