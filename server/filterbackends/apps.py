from django.apps import AppConfig


class FilterbackendsConfig(AppConfig):
    name = 'filterbackends'
